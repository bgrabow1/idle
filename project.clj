(defproject idle "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[buddy "2.0.0"]
                 [camel-snake-kebab "0.4.0"]
                 [clj-http "3.10.0"]
                 [clojure.java-time "0.3.2"]
                 [com.layerware/hugsql "0.4.9"]
                 [com.taoensso/tempura "1.2.1"]
                 [cprop "0.1.13"]
                 [expound "0.7.2"]
                 [hiccup "1.0.5"]
                 [hikari-cp "2.7.1"]
                 [io.replikativ/datahike "0.2.1"]
                 [juxt/crux "19.04-1.0.2-alpha"]
                 [lambdaisland/kaocha "0.0-418"]
                 [manifold "0.1.9-alpha3"]
                 [metosin/jsonista "0.2.2"]
                 [metosin/muuntaja "0.6.3"]
                 [metosin/reitit "0.2.13"]
                 [metosin/ring-http-response "0.9.1"]
                 [mount "0.1.16"]
                 [nrepl "0.6.0"]
                 [orchestra "2019.02.06-1"]
                 [org.clojure/clojure "1.10.1-beta2"]
                 [org.clojure/java.jdbc "0.7.9"]
                 [org.immutant/web "2.1.10"]
                 [org.postgresql/postgresql "42.2.5"]]
  :main ^:skip-aot idle.main
  :target-path "target/%s"

  :source-paths ["src"]
  :test-paths ["test"]
  :resource-paths ["resources"]

  :profiles
  {:uberjar {:aot :all}
   :repl {:repl-options {:init-ns user
                         :welcome (println "(dev) to load and switch to the 'dev' namespace.")}}
   :test {:dependencies [[lambdaisland/kaocha "0.0-418"]]}
   :dev {:dependencies [[expound "0.7.2"]
                        [prone "1.6.1"]]
         :source-paths ["dev/src"]
         :resource-paths ["dev/resources"]
         :jvm-opts ["-Dconfig=dev/dev.secret.edn"]}}
  :aliases {"test" ["with-profile" "+test" "run" "-m" "kaocha.runner"]})
