(ns idle.services.datahike
  (:require [datahike.api :as d]))

#_(def db-uri "datahike:file:///tmp/idle.datahike")
(def db-uri {:backend :file :path "local-db"})
(def mem-uri "datahike:mem://mem-example")

(comment
  #_(d/create-database db-uri)
  (d/delete-database mem-uri)
  )

(defn reset-db []
  (d/delete-database mem-uri)
  (d/create-database mem-uri)
  )

(d/create-database mem-uri)


(def conn (d/connect mem-uri))

@conn
