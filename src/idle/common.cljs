(ns idle.common
  (:require [re-frame.interceptor :as rf-interceptor]
            [re-frame.core :as rf]))

(def debug
  (rf-interceptor/->interceptor
   :id     :mydebug
   :before (fn debug-before
             [context]
             (println "Handling event:" (get-in context [:coeffects :event]))
             context)
   :after  (fn debug-after
             [context]
             (let [event   (get-in context [:coeffects :event])
                   orig-db (get-in context [:coeffects :db])
                   new-db  (get-in context [:effects :db] ::not-found)]
               (tap> {:event  (get-in context [:coeffects :event])
                      :before orig-db
                      :after  new-db})
               (if (= new-db ::not-found)
                 (println :log "No :db changes caused by:" event)
                 (do (println "before" orig-db)
                     (println "after" new-db)))
               context))))

(rf/reg-fx
 ::add-window-event-listener
 (fn [[event-type fn]]
   (.addEventListener js/window event-type fn)))

(rf/reg-fx
 ::remove-window-event-listener
 (fn [[event-type fn]]
   (.removeEventListener js/window event-type fn)))
