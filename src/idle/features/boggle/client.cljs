(ns idle.features.boggle.client
  (:require [kee-frame.core :as kf]
            [re-frame.core :as rf]
            [reagent.core :as r]
            [idle.common :as common]
            [idle.features.boggle.data :as boggle-data]
            [idle.features.boggle.game :as boggle-game]))



(rf/reg-fx
 ::shake-element-by-id
 (fn [[el-id]]
   (let [el (.getElementById js/document el-id)]
     (set! (.-className el) "shake")
     (.setTimeout js/window #(set! (.-className el) "") 250))))

(kf/reg-controller
 :route/boggle
 {:params (fn [route] (= (-> route :data :name) :route/boggle))
  :start [::initialize-boggle]
  :stop [::destroy-hanoi]})

(defn handle-enter-keypress [e]
  (when (= 13 (.-which e))
    (rf/dispatch [::add-word])))

(rf/reg-event-fx
 ::initialize-boggle
 [common/debug]
 (fn [_ _]
   {:db (boggle-data/create-game)
    ::common/add-window-event-listener ["keydown" handle-enter-keypress]}))

(rf/reg-event-fx
 ::destroy-boggle
 [common/debug]
 (fn [_ _]
   {::common/remove-window-event-listener ["keydown"  handle-enter-keypress]}))

(rf/reg-event-fx
 ::update-word
 [common/debug]
 (fn [{:keys [db]} [_ candidate]]
   (let [clean-candidate (.toUpperCase candidate)
         {:keys [error paths string]} (boggle-data/update-word db clean-candidate)]
     {:db (assoc db :error error :paths paths :string string)})))

(rf/reg-event-fx
 ::add-word
 [common/debug]
 (fn [{:keys [db]} _]
   (let [time-up? (:time-up db)
         {:keys [error word]} (boggle-game/validate-word db)
         next-db (-> db
                     (assoc :candidate [])
                     (assoc :string "")
                     (assoc :paths nil))]
     (cond error
           {:db (-> next-db (assoc :error error))
            :dispatch [::handle-invalid-word]}

           time-up?
           {:db db}

           :else
           {:db (-> next-db (update :words conj word))}))))

(rf/reg-event-fx
 ::handle-invalid-word
 [common/debug]
 (fn [{:keys [db]} _]
   {:db (dissoc db :error)
    ::shake-element-by-id ["wordentry"]}))

(rf/reg-event-db
 ::new-game
 [common/debug]
 (fn [_ _]
   (boggle-data/create-game)))

(rf/reg-event-db
 ::time-up
 (fn [db _]
   (assoc db :time-up true)))

(rf/reg-sub
 ::board
 (fn [db _]
   (:board db)))

(rf/reg-sub
 ::words
 (fn [db _]
   (reverse (:words db))))

(rf/reg-sub
 ::candidate
 (fn [db _]
   (reduce str (:candidate db))))

(rf/reg-sub
 ::string
 (fn [db _]
   (.toLowerCase (:string db))))

(rf/reg-sub
 ::error
 (fn [db _]
   (:error db)))

(rf/reg-sub
 ::score
 (fn [db _]
   (reduce (fn [score word] (+ score (boggle-game/word-score word)))
           0
           (:words db))))

(rf/reg-sub
 ::time-up
 (fn [db _]
   (:time-up db)))

(defn draw-timer []
  (let [time (r/atom (* 3 60))]
    (fn []
      (when (pos? @time)
        (js/setTimeout #(swap! time dec) 1000))
      (when (zero? @time)
        (rf/dispatch [::time-up]))
      [:div.timer
       (str (quot @time 60) ":"
            (let [seconds (rem @time 60)]
              (if (< seconds 10)
                (str "0" seconds)
                seconds)))])))

(defn draw-letter [[x y]]
  (let [board  (rf/subscribe [::board])]
    (fn []
      (let [letter (get-in @board [y x])] ; let needs to be inside otherwise it won't rerender with new board
        [:div.letter {:style {:transform (str "rotate(" (* 90 (rand-int 4)) "deg)")}}
         [:span (when (#{"Z" "N" "W" "M"} letter) {:style {:text-decoration "underline"}}) letter]]))))

(defn boggle []
  (let [words     (rf/subscribe [::words])
        candidate (rf/subscribe [::string])
        score     (rf/subscribe [::score])
        error     (rf/subscribe [::error])
        time-up   (rf/subscribe [::time-up])]
    (fn []
      [:div.boggle
       (if @time-up
         [:button.new-game {:on-click #(rf/dispatch [::new-game])} "NEW GAME"]
         [draw-timer])
       [:div.score {:style {:align-items "center"}} @score]
       [:div.board
        (doall
         (for [y (range 4) x (range 4)] ^{:key [x y]}
           [draw-letter [x y]]))]
       ;; TODO don't allow completions/autofill
       [:input#wordentry {:type      :text
                          :value     @candidate
                          :style     (when @error {:border "thick solid darkred"})
                          :on-change #(rf/dispatch [::update-word (.-value (.-target %))])}]

       [:ul.wordlist
        (for [word @words] ^{:key word}
          [:li word])]])))

(defn main!
  []
  (kf/start! {:routes [["/" :route/boggle]]
              :initial-db (boggle-data/create-game)
              :root-component [boggle]})
  (println "boggle!"))
