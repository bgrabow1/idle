(ns idle.features.boggle.game)

(defn validate-word
  [{:keys [words string] :as state}]
  (let [word (.toUpperCase string)
        long-enough? (>= (count word) 3)
        unique? (not ((set words) word))]
    (-> state
        (cond-> (not long-enough?)
          (update :error conj :word-must-have-at-least-three-letters))
        (cond-> (not unique?)
          (update :error conj :no-repeat-words))
        (cond-> (and long-enough? unique?)
          (assoc :word word)))))

(defn word-score [word]
  (if (< (count word) 5)
    1
    (count (drop 3 word))))
