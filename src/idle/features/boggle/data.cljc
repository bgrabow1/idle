(ns idle.features.boggle.data
  "Generate a board of letters, and try to find as many words as you can in the space of 3
  minutes."
  (:require [clojure.set :as set]))

(def cubes
  [["T" "W" "A" "O" "T" "O"]
   ["R" "H" "Z" "L" "N" "N"]
   ["O" "T" "M" "I" "C" "U"]
   ["D" "T" "Y" "T" "I" "S"]
   ["R" "T" "T" "E" "L" "Y"]
   ["A" "G" "A" "E" "N" "E"]
   ["A" "S" "P" "F" "K" "F"]
   ["O" "A" "B" "B" "O" "J"]
   ["I" "S" "S" "O" "T" "E"]
   ["U" "N" "H" "I" "M" "Qu"]
   ["E" "D" "L" "X" "R" "I"]
   ["S" "P" "A" "H" "O" "C"]
   ["H" "E" "E" "N" "G" "W"]
   ["R" "E" "T" "W" "H" "V"]
   ["E" "V" "Y" "R" "D" "L"]
   ["S" "U" "E" "E" "N" "I"]])

(defn roll [cube]
  (nth cube (rand-int 6)))

(defn rotate [letter]
  [letter (* (rand-int 4) 90)])

(defn gen-board []
  (->> (shuffle cubes)
       (map roll)
       (partition 4)
       (mapv vec)))

(defn neighbors [[x y]]
  (letfn [(n  [[x y]] [x (dec y)])
          (s  [[x y]] [x (inc y)])
          (e  [[x y]] [(inc x) y])
          (w  [[x y]] [(dec x) y])
          (ne [[x y]] [(inc x) (dec y)])
          (sw [[x y]] [(dec x) (inc y)])
          (nw [[x y]] [(dec x) (dec y)])
          (se [[x y]] [(inc x) (inc y)])]
    (->> ((juxt n e s w ne sw nw se) [x y])
         (remove (fn [[x y]] (or (neg? x) (neg? y) (> y 3) (> x 3))))
         (set))))

(defn letter-coords
  "Given a board and a letter, return all the x,y coordinates where the letter appears on
  the board. Returns an empty set if the letter doesn't appear on the board.

  Special treatment for Q: if we pass Q, look for Qu. This will allow us to not throw an
  error while someone is mid-typing."
  [board letter]
  (let [normalized-letter (if (= letter "Q") "Qu" letter)]
    (loop [y 0
           coords #{}]
      (if (= y 4)
        (set coords)
        (let [new-coords (reduce (fn [matches x]
                                   (if (= (get-in board [y x]) normalized-letter)
                                     (conj matches [x y])
                                     matches))
                                 #{}
                                 (range 4))]
          (recur (inc y) (set/union coords new-coords)))))))

 (defn create-game
  ([]
   (create-game (gen-board)))
  ([board]
   {:board board                        ; a vector of 4 4-element vectors
    :candidate []                       ; a vector of letters, possibly a word
    :string ""
    :paths nil
    :words []}))                        ; a vector of words

(defn valid-neighbor?
  "Valid if the letter is an untraversed neighbor to a path's tail."
  [path letter]
  (contains? (set/difference (neighbors (last path))
                             (set path))
             letter))

(defn new-paths
  "Returns a set of new paths through the board with the new letter's locations."
  [paths letter-locs]
  (set (mapcat (fn [path] (->> letter-locs
                               (map #(if (valid-neighbor? path %)
                                       (conj path %)
                                       path))
                               ;; remove all paths that aren't the right lenght
                               (filter #(> (count %) (count path)))))
               paths)))

;; could do coordinate validation only (click on letter)
(defn add-coordinate
  [{:keys [board candidate paths] :as state} letter-coord]
  (let [[x y]       letter-coord
        letter-locs #{letter-coord}
        letter      (get-in board [y x])
        next-paths  (new-paths paths letter-locs)]
    (cond (nil? paths)
          (-> state
              (assoc :paths (set (map vector letter-locs)))
              (assoc :candidate (conj candidate letter)))
          (not-empty next-paths)
          (-> state
              (assoc :paths next-paths)
              (assoc :candidate (conj candidate letter)))
          :else
          (-> state
              (assoc :error :invalid-letter)))))

(defn clean-candidate
  "If you've got a Q followed by a U, glom them together."
  [candidate]
  (->> (partition-all 2 1 (map str candidate))
       (reduce (fn [res [a b]]
                 (cond (and (= a "Q")
                            (= b "U"))
                       (conj res "Qu")
                       (and (= (last res) "Qu")
                            (= a "U"))
                       res
                       :else
                       (conj res a)))
               [])))

(defn update-word
  "When typing we don't know whether the next submission will have a letter at the end or
  in the middle or whether a letter is getting removed. So we re-validate the whole
  candidate.

  The string must always be updated, never prevent a user from typing."
  [{:keys [board] :as state} candidate]
  (if (= "" candidate)
    ;; reset the error state
    (-> state
        (assoc :string "")
        (dissoc :error))
    (let [candidate-letters (clean-candidate candidate)]

      (if (and ((set candidate-letters) "Q")
               (not= "Q" (last candidate-letters)))
        ;; If we have a Q that's not followed by a U
        (-> state
            (assoc :string (reduce str candidate-letters))
            (update :error conj [:invalid-letter :q-unfollowed-by-u "Q"]))
        ;; Validate the whole word
        (reduce
         (fn [{:keys [paths] :as state} letter]
           (let [letter-locs (letter-coords board letter)
                 next-paths (new-paths paths letter-locs)
                 next-state (update state :string str letter)]
             ;; permit invalid letters in candidate, but indicate there is a problem
             (cond  (empty? letter-locs) ; letter is not on the board
                    (-> next-state
                        (assoc paths next-paths)
                        (update :error conj [:invalid-letter :not-on-board letter]))
                    (nil? paths)        ; first letter
                    (-> next-state
                        (assoc :paths (set (map vector letter-locs))))
                    (not-empty next-paths) ; valid letter
                    (-> next-state
                        (assoc :paths next-paths))
                    :else               ; some other problem
                    (-> next-state
                        (update :error conj [:invalid-letter letter])
                        (assoc :paths next-paths)))))
         {:paths nil :string ""} ; <-- key bit, start from scratch each time
         candidate-letters)))))

(comment
  (letter-coords [["Qu" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]] "QA")
  #{}
  #{[0 0]}

  (update-word {:board [["Qu" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]]
                :paths nil :string ""} "OQA")


  (update-word {:board b :paths nil :string ""} "W")
)


(defn add-word [{:keys [candidate words] :as state}]
  (let [word (reduce str candidate)]
    (if (>= (count word) 3)
      (-> state
          (assoc :words (conj words word))
          (assoc :candidate [])
          (assoc :paths nil))
      (-> state
          (assoc :error :word-must-have-at-least-three-letters)))))

(comment
  ;; display a board
  (gen-board)

  ;; add a letter
  ;; validate it
  ;; ;; make sure it is on the board
  ;; ;; make sure it is adjacent to the last letter (if it's not the first)
  ;; ;; make sure it isn't a repeat

  ;; submit a word
  ;; validate it
  ;; ;; make sure it's at least 3 letters long
  ;; ;; make sure it's a dictionary word?

  (def b [["V" "O" "E" "N"]
          ["E" "U" "E" "M"]
          ["W" "C" "E" "W"]
          ["T" "I" "R" "A"]])


  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate ["W"],
   :paths {[0 2] [[0 2]], [3 2] [[3 2]]}}

  (create-game b)
  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate [],
   :paths nil,
   :words []}

  (-> (create-game b)
      (add-letter "W"))
  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate ["W"],
   :paths #{[[3 2]] [[0 2]]},
   :words []}

  (-> (create-game b)
      (add-letter "W")
      (add-letter "E"))
  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate ["W" "E"],
   :paths ([[3 2] [2 2]] [[3 2] [2 1]] [[0 2] [0 1]]),
   :words []}

  (-> (create-game b)
      (add-letter "W")
      (add-letter "E")
      (add-letter "A"))
  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate ["W" "E" "A"],
   :paths #{[[3 2] [2 2] [3 3]]},
   :words []}

  (-> (create-game b)
      (add-letter "W")
      (add-letter "E")
      (add-letter "A")
      (add-letter "R"))
  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate ["W" "E" "A" "R"],
   :paths #{[[3 2] [2 2] [3 3] [2 3]]},
   :words []}

  (-> (create-game b)
      (add-letter "W")
      (add-letter "E")
      (add-letter "A")
      (add-letter "R")
      (add-word))
  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate [],
   :paths nil,
   :words ["WEAR"]}

  (-> (create-game b)
      (add-letter "W")
      (add-letter "E")
      (add-letter "A")
      (add-letter "R")
      (add-word)
      (add-letter "C")
      (add-letter "R")
      (add-letter "E")
      (add-letter "W")
      (add-word))
  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate [],
   :paths nil,
   :words ["WEAR" "CREW"]}

  )
