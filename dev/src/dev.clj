(ns dev
  (:require [clojure.repl :refer :all]
            [clojure.tools.namespace.repl :as ns]
            [clojure.spec.alpha :as s]
            [orchestra.spec.test :as st]
            [expound.alpha :as expound]
            [mount.core :as mount]
            #_[idle.services.env :refer [env]]
            #_[idle.services.postgres :refer [postgres]]
            #_[idle.services.http-server :refer [http-server]]))

(alter-var-root #'s/*explain-out* (constantly expound/printer))

#_(defn start []
  (let [ret (mount/start (mount/only #_#{#'env #'http-server}))]
    (st/instrument)
    ret))

#_(defn stop []
  (mount/stop))

#_(defn reset []
  (mount/stop)
  (let [res (ns/refresh :after 'dev/start)]
    (st/instrument)
    res))

#_(defn reset-db []
  (migrations/migrate ["reset"] {:database-url (-> env :migration :database-url)}))

#_(defn migrate []
  (migrations/migrate ["migrate"] {:database-url (-> env :migration :database-url)}))

#_(defn rollback []
  (migrations/migrate ["rollback"] {:database-url (-> env :migration :database-url)}))

#_(defn create-migration [name]
  (migrations/create name {:database-url (-> env :migration :database-url)}))
